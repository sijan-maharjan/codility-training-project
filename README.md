# Codility Training Project
## This project is created to keep backup of training taken on [Codility Platform](https://app.codility.com/programmers)

This training helps to build solid skill for building algorithms.

It tests code output and complexity.

The codility report URL is provided on javadoc.

[Source Code](https://gitlab.com/sijan-maharjan/codility-training-project/-/tree/main/src/codility)

## Automated Backup
Those codes are pulled directly with the codility test url, the documentation on those codes are also pulled out of codility test results. The [PullCodility](https://gitlab.com/sijan-maharjan/codility-training-project/-/blob/main/src/codility/PullCodility.java) class is being used to pull those out directly using the URL, instead of having to manually maintain those files.
