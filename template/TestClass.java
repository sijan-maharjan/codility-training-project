package codility.template;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/* imports */

/**
 * <h1>${title}</h1>
 * ${synopsis}<br>
 * <br>
 * ${complexity}<br>
 * <br>
 * ${task_score}<br>
 * ${correctness}<br>
 * ${performance}<br>
 * Total Score ${total_score}<br>
 * <br>
 * @see <a href="${link_to_problem}">Problem Detail</a>
 * @see <a href="${link_to_report}">Codility report</a>
 * @version 2.0
 * @author ${author}
 * @since ${date}
 */
public class TestClass {

    /* code-block */

    @Test
    public void run( ) {
        /* test-cases */
    }
}
