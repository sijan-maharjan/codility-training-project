package codility.l12_euclidean_algorithm;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>ChocolatesByNumbers</h1>
 * There are N chocolates in a circle. Count the number of chocolates you will eat.<br>
 * <br>
 * <br>
 * <br>
 * Task Score 62%<br>
 * Correctness 100%<br>
 * Performance 25%<br>
 * Total Score 62%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/12-euclidean_algorithm/chocolates_by_numbers/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingT6WF2B-5NN/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-28
 */
public class ChocolatesByNumbers {

    public int solution(int N, int M) {
        int r = ( N > M )? N % M : M % N;
        if( N == 1 || r == 1 ) return N;
        if( r == 0 ) return N / M;
        int d = N/r;
        if( ( ( float ) d ) != ( (float) N / (float) r ) ) {
            return solution( N, r );
        }
        return r > 0? N/r: d;
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( 1, 2 ), "solution( 1, 2 )" );
		assertEquals( 5, solution( 10, 4 ), "solution( 10, 4 )" );
		assertEquals( 4, solution( 12, 21 ), "solution( 12, 21 )" );
    }
}

