package codility.l1_iterations;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>BinaryGap</h1>
 * Find longest sequence of zeros in binary representation of an integer.<br>
 * 
 * @see <a href="https://app.codility.com/programmers/lessons/1-iterations/binary_gap/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training5FBEQS-UGC/">Codility report</a>
 * @author Sijan
 * @since 2022-05-05
 */
public class BinaryGap {

    public int solution(int N) {
        return Arrays.stream(
                Integer.toBinaryString(N)
                .replace("0", " ")
                .trim()
                .split("1+")
        ).mapToInt(String::length)
                .max()
                .orElse(0);
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( 5 ), "solution( 5 )" );
    }
}

