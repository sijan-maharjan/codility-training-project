package codility;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.print.PrintOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class can be used to pull down the work & report from codility with ease and store locally in an organized way<br>
 * Downloads all code-works and creates an equivalent class locally which can be run with test cases any time<br>
 * Downloads the full report from codility
 *
 * uses /template/TestClass.java - template class to generate new class
 * @author Sijan Maharjan
 * @version 2.1 <br>
 * - now downloads full-sized pdf report<br>
 * - the pdf report contain link to report URL on reportId
 */
public class PullCodility {

    /* cmdline-args: <problem_url> <report_url> [...<test_params>] */
    /* test-param: <method_input_params>\t<expected_return>[\t<override_method_name>] */    // used for junit testing
                                                                                            // default method called for testing: solution();
    // if cmdline-args are not passed, you may also choose to enter those in an interactive way

    static int[] pageDimension = new int[]{ 1920, 3151 };   // expected report webpage size
    static boolean rejectIfClassExists = true;              // if false, same problem can be downloaded more than once
    private static String[] parameters;

    public static void main( String ... args ) throws IOException {
        try{
            getParameters( args );
            System.out.println("\nreading parameters...");
            populateInterestedProperties( );
            downloadFullReport( );
            createNewClass( );
        } catch ( IllegalArgumentException ex ){
            System.err.println( ex.getMessage( ) );
        }
    }

    private static void getParameters( String ... args ) {
        try ( Scanner sc = new Scanner( System.in ) ) {
            if ( args.length > 0 ) {
                parameters = args;
                overridePageDimension( sc );
            } else {
                getParametersInteractiveWay( sc );
            }
        }
    }

    private static void getParametersInteractiveWay( Scanner sc ) throws IllegalArgumentException {

        System.out.print("Problem URL: ");
        String problemURL = getValidProblemUrl( sc );

        System.out.print("Result URL: ");
        String reportURL = getValidReportUrl( sc );

        List<String> testParams = new ArrayList<>();
        System.out.println("test params [<test_args><tab><expected_result><tab><method_name?>]: ");
        String line;
        do{
            line = sc.nextLine().trim();
            if( !line.isBlank() ) testParams.add( line );
        }while( !line.isBlank() );

        parameters = new String[2+testParams.size()];
        parameters[0] = problemURL;
        parameters[1] = reportURL;
        for( int i=2, j=0; j<testParams.size(); i++, j++ ){
            parameters[i] = testParams.get(j);
        }

        overridePageDimension( sc );

    }

    private static String getValidProblemUrl( Scanner sc ) throws IllegalArgumentException {
        String url = sc.nextLine( );
        if( url.isBlank()
            || !url.contains("/app.codility.com/")
            || (!url.contains("/lessons/") && !url.contains("/trainings/"))
        ){
            throw new IllegalArgumentException( "Valid URL Required" );
        }
        return url;
    }

    private static String getValidReportUrl( Scanner sc ) throws IllegalArgumentException {
        String url = sc.nextLine( );
        if( url.isBlank()
                || !url.contains("/app.codility.com/")
                || !url.contains("/results/")
        ){
            throw new IllegalArgumentException( "Finish Your Problem First!" );
        }
        return url;
    }

    private static void overridePageDimension( Scanner sc ) {
        System.out.print("Report Dimension ( default: " + pageDimension[0] + " x " + pageDimension[1] + " ): ");
        String dimension = sc.nextLine();
        if (!dimension.isBlank()) {
            String[] dim = dimension.split("x");
            pageDimension[0] = (dim[0].trim().isBlank()) ? pageDimension[0] : Integer.parseInt(dim[0].trim());
            pageDimension[1] = (dim[1].trim().isBlank()) ? pageDimension[1] : Integer.parseInt(dim[1].trim());
        }
    }

    static boolean done = false;
    private static void downloadFullReport( ) throws IOException {
        System.out.println("Downloading full report...");
        String fileName = "Full Report - "+InterestedProperties.folder+"-"+InterestedProperties.className+".pdf";
        File outputFile = new File("test-results", fileName);
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless", "--window-size="+pageDimension[0]+","+pageDimension[1]);

        WebDriver driver = new ChromeDriver( options );

        Dimension newDimension = new Dimension( pageDimension[0], pageDimension[1] );
        driver.manage().window().setSize( newDimension );

        System.out.println("\nHold Tight...");

        new Thread(()->{
            int waited = 0;
            while(!done){
                System.out.print("time-lapsed: "+(waited++)+"s\r");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();

        //Access the targetted URL
        driver.get( InterestedProperties.reportURL );
        WebElement element = new WebDriverWait( driver, Duration.ofSeconds( 30 ) ).until(
                ExpectedConditions.presenceOfElementLocated((By.cssSelector("span[data-test-id=score_large_value]"))));

        InterestedProperties.totalScore = element.getText();

        WebElement reportId = new WebDriverWait( driver, Duration.ofSeconds( 30 ) ).until(
                ExpectedConditions.presenceOfElementLocated((By.cssSelector("span[data-test-id=cr_name]"))));

        String linkedReportId = "<a href = '"+InterestedProperties.reportURL+"'>"+reportId.getText()+"</a>";

        JavascriptExecutor js = ( JavascriptExecutor ) driver;
        js.executeScript( "document.querySelector('span[data-test-id=cr_name]').innerHTML = \""+linkedReportId+"\";" );

        Pdf pdf = ( ( PrintsPage ) driver ).print( new PrintOptions( ) );

        Files.write( Paths.get( outputFile.getPath( ) ), OutputType.BYTES.convertFromBase64Png( pdf.getContent( ) ) );

        System.out.println( "Pulled - "+outputFile.getPath( ) );

        while (!done) {
            try {
                done = true;
            }catch (Exception e){};
        }

        //Quit the driver
        driver.quit();
    }

    private static void createNewClass( ) throws FileNotFoundException {

        File dir = new File( pkgToDir(InterestedProperties.packageName) );
        if(!dir.exists()){
            dir.mkdirs();
        }

        String synopsis = InterestedProperties.synopsis;
        synopsis = Arrays.stream(synopsis.split("\n")).map(s->" * "+s+"<br>\n").collect(Collectors.joining());
        synopsis = synopsis.substring(0,synopsis.length()-1);

        StringBuilder finalCode = new StringBuilder();
        try(Scanner sc = new Scanner(new File( "template", classToFileName("TestClass")))){
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                line = line.trim().startsWith("package ")?"package "+InterestedProperties.packageName+";":line;
                line = line.replace("/* imports */", InterestedProperties.imports);
                line = line.replace("${title}", InterestedProperties.title);
                line = line.replace("${class_name}", InterestedProperties.className)
                                    .replace("TestClass", InterestedProperties.className);
                line = line.contains( "${synopsis}" )? synopsis: line;
                line = line.replace("${link_to_problem}", InterestedProperties.problemURL);
                line = line.replace("${link_to_report}", InterestedProperties.reportURL);
                line = line.replace("${author}", System.getProperty("user.name"));
                line = line.replace("${date}", LocalDate.now().toString());
                line = line.replace("/* code-block */", InterestedProperties.codeBlock);
                line = InterestedProperties.testCases != null && line.contains("/* test-cases */")? InterestedProperties.testCases:line;
                line = line.replace("${complexity}", InterestedProperties.complexity);
                line = line.replace("${task_score}", InterestedProperties.taskScore);
                line = line.replace("${correctness}", InterestedProperties.correctness);
                line = line.replace("${performance}", InterestedProperties.performance);
                line = line.replace("${total_score}", InterestedProperties.totalScore);
                finalCode.append( line ).append("\n");
            }
        }

        try( PrintWriter out = new PrintWriter( getFile() ) ){
            out.println( finalCode );
        }

    }

    private static String getImports( String code ) {
        StringBuilder imports = new StringBuilder();
        for(String line: code.split("\n")){
            if(line.trim().startsWith("import ")){
                imports.append(line).append("\n");
            }
        }
        String ims = imports.toString();
        return ims.isBlank()?"": ims.substring(0,ims.length()-1);
    }

    private static String getInterestedCodeBlock(String code) {
        boolean interested = false;
        StringBuilder block = new StringBuilder();
        for(String line: code.split("\n")){
            if(interested && !line.trim().isBlank()){
                block.append(line).append("\n");
            }
            if(line.trim().startsWith("class ")){
                interested = true;
            }
        }
        String interestedBlock = block.toString();
        return interestedBlock.substring( 0, interestedBlock.length()-3 );
    }

    private static File getFile( ){
        return new File( pkgToDir( InterestedProperties.packageName ), classToFileName( InterestedProperties.className ) );
    }

    private static String pkgToDir( String pkg ){
        return System.getProperty("user.dir") + File.separator +  "src" + File.separator + pkg.replaceAll("\\.", "/");
    }

    private static String classToFileName( String className ){
        return className + ".java";
    }

    private static void populateInterestedProperties() throws IOException {
        InterestedProperties.problemURL = parameters[0].trim();
        InterestedProperties.reportURL = parameters[1].trim();
        parsePackageAndClassName();
        if( parameters.length>2 ) populateTestCases();

        Document doc = Jsoup.connect(InterestedProperties.problemURL).get();
        String expectedTitle = doc.select("h4.title a").text().trim();
        InterestedProperties.synopsis = doc.select("div.current_lesson div.synopsis").text().trim();


        Document reportDoc = Jsoup.connect(InterestedProperties.reportURL).get();
        String title = reportDoc.select("div.task-opener a").text().trim();

        if( !expectedTitle.equals( title ) ){
            System.err.println("Unrelated Report Detected!");
            System.exit(0);
        }

        InterestedProperties.title = title;

        Elements codeBox = reportDoc.select("div.code-highlight code");
        String code = Objects.requireNonNull(codeBox.last()).text( );
        InterestedProperties.imports = getImports( code ).trim();
        InterestedProperties.codeBlock = getInterestedCodeBlock( code ).trim();
        InterestedProperties.complexity = reportDoc.select("div.analysis div.detected-time-print").text( ).trim( );
        Elements resultCells = reportDoc.select("div.task-result-cell");
        InterestedProperties.taskScore = resultCells.get( 0 ).text( ).trim( );
        InterestedProperties.correctness = resultCells.get( 1 ).text( ).trim( );
        InterestedProperties.performance = resultCells.get( 2 ).text( ).trim( );
        InterestedProperties.totalScore = "N/A";
    }

    private static void populateTestCases() {
        StringBuilder cases = new StringBuilder();
        for(int i=2; i< parameters.length; i++){
            String[] testParam = parameters[i].split("\t");
            String methodName = "solution"; boolean isAssertMethod = false;
            if( testParam.length > 2){
                methodName = testParam[2];
                isAssertMethod = methodName.startsWith( "assert" );
            }

            if( isAssertMethod ) {
                if( testParam[1].isBlank()){
                    cases.append("\t\t").append("solution").append("(").append(testParam[0]).append(");\n");
                } else {
                    cases.append("\t\t").append(methodName).append("( ").append(testParam[1]).append(", ").append("solution").append("( ").append(testParam[0]).append(" )").append(", \"").append("solution").append("( ").append(testParam[0].replaceAll("\"","\\\\\"")).append(" )\" );\n");
                }
            } else {
                if( testParam[1].isBlank()){
                    cases.append("\t\t").append(methodName).append("(").append(testParam[0]).append(");\n");
                } else {
                    cases.append("\t\tassertEquals( ").append(testParam[1]).append(", ").append(methodName).append("( ").append(testParam[0]).append(" )").append(", \"").append(methodName).append("( ").append(testParam[0].replaceAll("\"","\\\\\"")).append(" )\" );\n");
                }
            }
        }
        String testCases = cases.toString();
        testCases = testCases.substring(0, testCases.length()-1);
        InterestedProperties.testCases = testCases;

    }

    private static void parsePackageAndClassName() {
        String url = InterestedProperties.problemURL;
        if(url.endsWith("/"))url = url.substring(0,url.length()-1);
        String[] url_parts = url.split("/");
        int part_length = url_parts.length;
        if(url.contains("/trainings/")) {
            InterestedProperties.folder = "exercise"+ url_parts[part_length - 2].replace("-", "_");
        }else {
            InterestedProperties.folder = "l" + url_parts[part_length - 2].replace("-", "_");
        }
        InterestedProperties.packageName = "codility."+InterestedProperties.folder;
        String[] raw_class = url_parts[part_length-1].split("_");
        String className = Arrays.stream(raw_class)
                .map(p->p.replaceAll("[^a-zA-Z0-9]", ""))
                .map( p->p.substring(0,1).toUpperCase()+p.substring(1).toLowerCase() )
                .collect(Collectors.joining());
        className = Character.isDigit(className.charAt(0))? "C"+className:className;
        InterestedProperties.className = className;
        int i = 2;
        while( getFile().exists() && getFile().isFile() ){
            if(rejectIfClassExists){
                System.err.println(InterestedProperties.packageName+"."+InterestedProperties.className+" already exists!");
                System.exit(0);
            }
            InterestedProperties.className = className + "X"+(i++);
        }
    }


    protected static class InterestedProperties {
        public static String folder;
        public static String packageName;
        public static String className;
        public static String problemURL;
        public static String reportURL;
        public static String synopsis;
        public static String imports;
        public static String codeBlock;
        public static String title;
        public static String testCases;
        public static String complexity;
        public static String taskScore;
        public static String correctness;
        public static String performance;
        public static String totalScore;
    }
}
