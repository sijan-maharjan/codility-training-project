package codility.l3_time_complexity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>FrogJmp</h1>
 * Count minimal number of jumps from position X to Y.<br>
 * Detected time complexity: O(1)
 * @see <a href="https://app.codility.com/programmers/lessons/3-time_complexity/frog_jmp/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingWFXPF9-G57/">Codility report</a>
 * @author Sijan
 * @since 2022-05-05
 */
public class FrogJmp {

    public int solution(int X, int Y, int D) {
        int totalJmp = (Y - X) / D;
        return  (X + totalJmp * D == Y)? totalJmp : totalJmp+1;
    }

    @Test
    public void run( ) {
        /* test-cases */
    }
}

