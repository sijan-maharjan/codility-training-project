package codility.l3_time_complexity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


/**
 * <h1>PermMissingElem</h1>
 * Find the missing element in a given permutation.<br>
 * Detected time complexity: O(N) or O(N * log(N))
 * @see <a href="https://app.codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingB7BYRZ-YYG/">Codility report</a>
 * @author Sijan
 * @since 2022-05-06
 */
public class PermMissingElem {

    public int solution(int[] A) {
        int size = A.length;
        if(A.length == 0) return 1;
        int sum = 0, expected = 0;
        for( int i =1; i <= size; i++ ){
            expected += i;
            sum += A[i-1];
        }
        return size + ( expected - sum ) + 1;
    }

    @Test
    public void run( ) {
        assertEquals( 4, solution( new int[]{3,2,1,5} ), "solution( new int[]{3,2,1,5} )" );
    }
}

