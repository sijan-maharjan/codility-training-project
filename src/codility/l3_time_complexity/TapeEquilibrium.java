package codility.l3_time_complexity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


/**
 * <h1>TapeEquilibrium</h1>
 * Minimize the value |(A[0] + ... + A[P-1]) - (A[P] + ... + A[N-1])|.<br>
 * Detected time complexity: O(N)
 * @see <a href="https://app.codility.com/programmers/lessons/3-time_complexity/tape_equilibrium/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingZHHFRE-WMQ/">Codility report</a>
 * @author Sijan
 * @since 2022-05-06
 */
public class TapeEquilibrium {

    public int solution(int[] A) {
        int size = A.length;
        int[][] list = new int[size-1][2];
        list[0][0] = A[0];
        list[0][1] = A[size-1];
        int sumX=list[0][0],sumY=list[0][1];
        for (int i=1, j=size-2; i<size-1 && j>0; i++, j++){
            sumX += A[i];
            sumY += A[size-i-1];
            list[i][0] = sumX;
            list[i][1] = sumY;
        }
        int min = Integer.MAX_VALUE;
        for (int i=0; i<list.length; i++){
            int diff = Math.abs( list[i][0] - list[list.length-i-1][1] );
            if(diff<min) min = diff;
        }
        return min;
    }

    @Test
    public void run( ) {
        assertEquals( 77, solution( new int[]{3,1,2,-100,1,2,5,8,4,3} ), "solution( new int[]{3,1,2,-100,1,2,5,8,4,3} )" );
        assertEquals( 4000, solution( new int[]{-2000, 2000} ), "solution( new int[]{-2000, 2000} )" );
        assertEquals( 0, solution( new int[]{2000, 2000} ), "solution( new int[]{2000, 2000} )" );
        assertEquals( 1, solution( new int[]{3,1,2,4,3} ), "solution( new int[]{3,1,2,4,3} )" );
        assertEquals( 1, solution( new int[]{-3,-1,-2,-4,-3} ), "solution( new int[]{-3,-1,-2,-4,-3} )" );
        assertEquals( 995, solution( new int[]{-3000,0,5,1000,-3000} ), "solution( new int[]{-3000,0,5,1000,-3000} )" );
    }
}

