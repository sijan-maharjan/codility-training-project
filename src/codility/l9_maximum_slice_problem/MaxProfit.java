package codility.l9_maximum_slice_problem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>MaxProfit</h1>
 * Given a log of stock prices compute the maximum possible earning.<br>
 * <br>
 * Detected time complexity: O(N)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/9-maximum_slice_problem/max_profit/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingTJ9EGK-RAS/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-08-07
 */
public class MaxProfit {

    public int solution(int[] A) {
        int minCost = Integer.MAX_VALUE, maxCost = Integer.MIN_VALUE;
        Stack<Integer> profits = new Stack<>();
        for( int i=0; i< A.length; i++ ){
            if( A[i] < minCost ) {
                minCost = A[i];
                maxCost = Integer.MIN_VALUE;
            }else if( A[i] > maxCost ) {
                maxCost = A[i];
            }
            if( minCost != Integer.MAX_VALUE && maxCost != Integer.MIN_VALUE ) {
                int profit = maxCost - minCost;
                if ( profit > 0 && !profits.contains( profit ) && ( profits.isEmpty( ) || profit > profits.lastElement( ) ) ) {
                    profits.push(profit);
                }
            }
        }
        return profits.isEmpty( ) ? 0: profits.pop( );
    }

    @Test
    public void run( ) {
		assertEquals( 356, solution( new int[]{ 23171, 21011, 21123, 21366, 21013, 21367 } ), "solution( new int[]{ 23171, 21011, 21123, 21366, 21013, 21367 } )" );
		assertEquals( 1005, solution( new int[]{ 5,1010,2, 1000 } ), "solution( new int[]{ 5,1010,2, 1000 } )" );
    }
}

