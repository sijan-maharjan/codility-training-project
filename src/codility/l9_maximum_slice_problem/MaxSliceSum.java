package codility.l9_maximum_slice_problem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>MaxSliceSum</h1>
 * Find a maximum sum of a compact subsequence of array elements.<br>
 * <br>
 * Detected time complexity: O(N)<br>
 * <br>
 * Task Score 84%<br>
 * Correctness 87%<br>
 * Performance 80%<br>
 * Total Score 84%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/9-maximum_slice_problem/max_slice_sum/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingWN4XPX-FAH/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-18
 */
public class MaxSliceSum {

    public int solution(int[] A) {
        int sum = 0, max = Integer.MIN_VALUE, hold1 = sum, hold2 = hold1;
        for( int i=0; i< A.length; i++ ){
            if( i != 0 && (( A[i-1] < 0 && A[i] > 0 ) || ( A[i-1] > 0 && A[i] < 0 )) ){
                hold2 = hold1;
                hold1 = sum;
                sum = 0;
            }
            sum += A[i];
            if( sum > max ) {
                max = sum;
            }
            if( ( sum + hold1 + hold2 ) > max ) {
                sum = ( sum + hold1 + hold2 );
                max = sum;
            }
        }
        return max;
    }

    @Test
    public void run( ) {
		assertEquals( 5, solution( new int[]{ 3,2,-6,4,0 } ), "solution( new int[]{ 3,2,-6,4,0 } )" );
		assertEquals( 5, solution( new int[]{ 5 } ), "solution( new int[]{ 5 } )" );
		assertEquals( 4, solution( new int[]{ 3, -2, 3 } ), "solution( new int[]{ 3, -2, 3 } )" );
    }
}

