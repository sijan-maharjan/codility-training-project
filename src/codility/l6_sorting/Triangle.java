package codility.l6_sorting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>Triangle</h1>
 * Determine whether a triangle can be built from a given set of edges.<br>
 * <br>
 * Detected time complexity: O(N**3)<br>
 * <br>
 * Task Score 68%<br>
 * Correctness 90%<br>
 * Performance 33%<br>
 * Total Score 68%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/6-sorting/triangle/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingR557D4-6XQ/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-14
 */
public class Triangle {

    public int solution(int[] A) {
        for (int i = 0; i < A.length-2; i++) {
            for (int j = i+1; j < A.length-1; j++) {
                for (int k = j+1; k < A.length; k++) {
                    if( A[i] + A[j] > A[k] && A[k] + A[j] > A[i] && A[i] + A[k] > A[j] ) return 1;
                }
            }
        }
        return 0;
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( new int[]{ 10,2,5,1,8,20 } ), "solution( new int[]{ 10,2,5,1,8,20 } )" );
		assertEquals( 0, solution( new int[]{ 10,50,5,1 } ), "solution( new int[]{ 10,50,5,1 } )" );
		assertEquals( 0, solution( new int[]{ 5,3,3 } ), "solution( new int[]{ 5,3,3 } )" );
    }
}

