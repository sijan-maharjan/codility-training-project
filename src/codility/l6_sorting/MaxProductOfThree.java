package codility.l6_sorting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>MaxProductOfThree</h1>
 * Maximize A[P] * A[Q] * A[R] for any triplet (P, Q, R).<br>
 * <br>
 * Detected time complexity: O(N * log(N))<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/6-sorting/max_product_of_three/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingDJGC7H-FK5/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-11
 */
public class MaxProductOfThree {

    public int solution(int[] A) {
        Arrays.sort(A);
        int s = A.length;
        int prod1 = A[s-1] * A[s-2] * A[s-3];
        int prod2 = A[0] * A[1] * A[2];
        int prod3 = A[0] * A[1] * A[s-1];
        int max = Math.max(Math.max(prod1, prod2), prod3);
        if( s>3 ){
            int prod4 = A[0] * A[2] * A[s-1];
            int prod5 = A[1] * A[2] * A[s-1];
            return Math.max(Math.max(max, prod4),prod5);
        }
        return max;
    }

    @Test
    public void run( ) {
		assertEquals( 60, solution( new int[]{-3,1,2,-2,5,6} ), "solution( new int[]{-3,1,2,-2,5,6} )" );
		assertEquals( -80, solution( new int[]{-10, -2, -4} ), "solution( new int[]{-10, -2, -4} )" );
    }
}

