package codility.l6_sorting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>Distinct</h1>
 * Compute number of distinct values in an array.<br>
 * <br>
 * Detected time complexity: O(N*log(N)) or O(N)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/6-sorting/distinct/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training6R3PFY-XW9/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-11
 */
public class Distinct {

    public int solution(int[] A) {
        int s = A.length;
        Set<Integer> set = new HashSet<>();
        for(int i=0; i<s/2; i++){
            set.add(A[i]);
            set.add(A[s-i-1]);
        }
        if(s%2 != 0){
            set.add(A[s/2]);
        }
        return set.size();
    }

    @Test
    public void run( ) {
		assertEquals( 5, solution( new int[]{4,2,2,5,1,5,8} ), "solution( new int[]{4,2,2,5,1,5,8} )" );
		assertEquals( 1, solution( new int[]{1} ), "solution( new int[]{1} )" );
    }
}

