package codility.l8_leader;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>EquiLeader</h1>
 * Find the index S such that the leaders of the sequences A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N - 1] are the same.<br>
 * <br>
 * Detected time complexity: O(N ** 2)<br>
 * <br>
 * Task Score 55%<br>
 * Correctness 100%<br>
 * Performance 0%<br>
 * Total Score 55%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/8-leader/equi_leader/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training5TTVH2-NAD/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-06-05
 */
public class EquiLeader {

    public int solution(int[] A) {
        int c = 0;
        for( int i=0; i< A.length-1; i++ ){
            Map<Integer, Integer> m1 = new HashMap<>();
            int[] x1 = new int[]{ -1, 0, -1 };
            for( int j=0; j<= i; j++ ) {
                m1.put(A[j], Optional.ofNullable(m1.get(A[j])).orElse(0) + 1);
                if (m1.get(A[j]) > x1[1]) {
                    x1[0] = j;
                    x1[1] = m1.get(A[j]);
                    x1[2] = A[j];
                }
            }
            int v1 = x1[1] > ( (i+1)/2 )? x1[0]: -1;
            if( v1 != -1 ){
                int[] x2 = new int[]{ -1, 0, -1 };
                Map<Integer, Integer> m2 = new HashMap<>();
                for( int j=i+1; j< A.length; j++ ) {
                    m2.put(A[j], Optional.ofNullable(m2.get(A[j])).orElse(0) + 1);
                    if (m2.get(A[j]) > x2[1]) {
                        x2[0] = j;
                        x2[1] = m2.get(A[j]);
                        x2[2] = A[j];
                    }
                }
                int v2 = x2[1] > ( (A.length-i-1)/2 )? x2[0]: -1;
                if( v2 != -1 && x1[2] == x2[2] )
                    c++;
            }
        }
        return c;
    }

    @Test
    public void run( ) {
		assertEquals( 2, solution( new int[]{ 4, 3, 4, 4, 4, 2 } ), "solution( new int[]{ 4, 3, 4, 4, 4, 2 } )" );
    }
}

