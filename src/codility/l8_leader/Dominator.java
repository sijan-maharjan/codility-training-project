package codility.l8_leader;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>Dominator</h1>
 * Find an index of an array such that its value occurs at more than half of indices in the array.<br>
 * <br>
 * Detected time complexity: O(N*log(N)) or O(N)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/8-leader/dominator/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingWCF7VH-Q27/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-06-05
 */
public class Dominator {

    public int solution(int[] A) {
        int[] x = new int[]{ -1, 0 };
        Map<Integer, Integer> m = new HashMap<>();
        for( int i=0; i< A.length; i++ ){
            m.put( A[i], Optional.ofNullable( m.get(A[i]) ).orElse( 0 ) + 1 );
            if( m.get( A[i] ) > x[1] ) {
                x[0] = i;
                x[1] = m.get( A[i] );
            }
        }
        return x[1] > ( A.length/2 )? x[0]: -1;
    }

    @Test
    public void run( ) {
		assertEquals( 7, solution( new int[]{ 3,4,3,2,3,-1,3,3 } ), "solution( new int[]{ 3,4,3,2,3,-1,3,3 } )" );
    }
}

