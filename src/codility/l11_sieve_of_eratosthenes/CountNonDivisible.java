package codility.l11_sieve_of_eratosthenes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>CountNonDivisible</h1>
 * Calculate the number of elements of an array that are not divisors of each element.<br>
 * <br>
 * Detected time complexity: O(N ** 2)<br>
 * <br>
 * Task Score 55%<br>
 * Correctness 100%<br>
 * Performance 0%<br>
 * Total Score 55%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/11-sieve_of_eratosthenes/count_non_divisible/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training2GH5X5-5HQ/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-27
 */
public class CountNonDivisible {

    public int[] solution(int[] A) {
        int[] n = new int[A.length];
        int x = n.length -1;
        List<Integer> f = new ArrayList<>( );
        f.add( 0 );
        for(int j =1, l = 0; j< n.length/2 + ( n.length%2==0?0:1); j++, l=0) {
            if( !f.isEmpty() ) {
                do {
                    int i = f.get( l++ );
                    if( A[i] % A[j] != 0 ) {
                        n[i] += 1;
                    }
                    if( A[j] % A[i] != 0 ) {
                        n[j] += 1;
                    }
                    int p = x-i, q = x-j;
                    if( A[p] % A[q] != 0 ) {
                        n[p] += 1;
                    }
                    if( A[q] % A[p] != 0 ) {
                        n[q] += 1;
                    }
                } while ( l< f.size( ) );
            }
            f.add( j );
        }
        if( !f.isEmpty() ) {
            for(int k =0, l = 0; k< f.size()-( n.length%2==0?0:1); k++, l=0) {
                int i = f.get(k);
                do {
                    int j = x - f.get( l++ );
                    if(f.contains(j)) continue;
                    if( A[i] % A[j] != 0 ) {
                        n[i] += 1;
                    }
                    if( A[j] % A[i] != 0 ) {
                        n[j] += 1;
                    }
                } while ( l< f.size() );
            }
        }
        return n;
    }

    @Test
    public void run( ) {
		assertArrayEquals( new int[]{ 2,4,3,2,0 }, solution( new int[]{ 3,1,2,3,6 } ), "solution( new int[]{ 3,1,2,3,6 } )" );
		assertArrayEquals( new int[]{ 0 }, solution( new int[]{ 3 } ), "solution( new int[]{ 3 } )" );
        assertArrayEquals( new int[]{ 2,2,3,2 }, solution( new int[]{ 3, 5, 1, 2 } ), "solution( new int[]{ 3, 5, 1, 2 } )" );
		assertArrayEquals( new int[]{ 1, 0 }, solution( new int[]{ 3, 6 } ), "solution( new int[]{ 3, 6 } )" );
    }
}

