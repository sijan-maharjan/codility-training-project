package codility;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Scratch {

    public int solution( int [] A ) {
        return (int) Arrays.stream(A).parallel( ).map(Math::abs).distinct().count();
    }

    @Test
    public void run( ){
        assertEquals( 5, solution( new int[]{-5,-3,-1,0,3,6} ) );
    }

}
