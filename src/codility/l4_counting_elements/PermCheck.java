package codility.l4_counting_elements;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>PermCheck</h1>
 * Check whether array A is a permutation.<br>
 * Detected time complexity: O(N) or O(N * log(N))
 * @see <a href="https://app.codility.com/programmers/lessons/4-counting_elements/perm_check/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training4BHSFN-TRW/">Codility report</a>
 * @author Sijan
 * @since 2022-05-07
 */
public class PermCheck {

    public int solution(int[] A) {
        Arrays.sort(A);
        for(int i = 1; i<= A.length; i++ ){
            if( A[i-1] != i ) return 0;
        }
        return 1;
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( new int[]{4,1,2,3} ), "solution( new int[]{4,1,2,3} )" );
		assertEquals( 0, solution( new int[]{4,1,3} ), "solution( new int[]{4,1,3} )" );
    }
}

