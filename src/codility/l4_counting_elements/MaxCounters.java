package codility.l4_counting_elements;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>MaxCounters</h1>
 * Calculate the values of counters after applying all alternating operations: increase counter by 1; set value of all counters to current maximum.<br>
 * Detected time complexity: O(N + M)
 * @see <a href="https://app.codility.com/programmers/lessons/4-counting_elements/max_counters/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingKZPRVC-WGH/">Codility report</a>
 * @author Sijan
 * @since 2022-05-07
 */
public class MaxCounters {

    public int[] solution(int N, int[] A) {
        int max = -1; int threshold = 0;
        int[] counter = new int[N];
        for( int x: A ){
            if( x >= 1 && x <= N ) {
                counter[x-1] = Math.max( counter[x - 1], threshold ) + 1;
                if( counter[x-1] > max )
                    max = counter[x-1];
            } else {
                threshold = max;
            }
        }
        for( int i = 0; i < N; i++ ){
            counter[i] = Math.max( counter[i], threshold );
        }
        return counter;
    }

    @Test
    public void run( ) {
		assertArrayEquals( new int[]{3, 2, 2, 4, 2}, solution( 5, new int[]{3,4,4,6,1,4,4} ), "solution( new int[]{3,4,4,6,1,4,4}, 5 )" );
    }
}

