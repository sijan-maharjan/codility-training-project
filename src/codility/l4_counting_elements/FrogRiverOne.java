package codility.l4_counting_elements;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * <h1>FrogRiverOne</h1>
 * Find the earliest time when a frog can jump to the other side of a river.<br>
 * Detected time complexity: O(N)
 * @see <a href="https://app.codility.com/programmers/lessons/4-counting_elements/frog_river_one/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingV8KDF7-7BS/">Codility report</a>
 * @author Sijan
 * @since 2022-05-06
 */
public class FrogRiverOne {

    public int solution(int X, int[] A) {
        Set<Integer> leaves = new HashSet<>();
        int maxWait = -1, stepCount = 0;
        for(int i=0;i<A.length;i++){
            if( !leaves.contains( A[i] ) ){
                leaves.add(A[i]);
                if( A[i] <= X && i > maxWait ){
                    maxWait = i;
                    stepCount++;
                }
            }
        }
        return stepCount == X? maxWait: -1;
    }

    @Test
    public void run( ) {
		assertEquals( 8, solution( 6, new int[]{1,2,3,1,4,2,4,5,6,8} ), "solution( 6, new int[]{1,2,3,1,4,2,4,5,6,8} )" );
    }
}

