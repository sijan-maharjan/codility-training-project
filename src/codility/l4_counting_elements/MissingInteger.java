package codility.l4_counting_elements;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>MissingInteger</h1>
 * Find the smallest positive integer that does not occur in a given sequence.<br>
 * <br>
 * Detected time complexity: O(N) or O(N * log(N))<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/4-counting_elements/missing_integer/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingS2QCF7-UD2/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-07
 */
public class MissingInteger {

    public int solution(int[] A) {
        Arrays.sort(A);
        int max = 0;
        for( int i = 1; i<= A.length; i++ ){
            int l = (i-2<0||A[i-2]<0?0:A[i-2]);
            if( A[i-1] > (l+1) && (l+1) > 0 ){
                return l+1;
            }
            if( A[i-1] > max )
                max = A[i-1];
        }
        return max + 1;
    }

    @Test
    public void run( ) {
		assertEquals( 5, solution( new int[]{ 1, 3, 6, 4, 1, 2 } ), "solution( new int[]{ 1, 3, 6, 4, 1, 2 } )" );
		assertEquals( 4, solution( new int[]{ 1, 2, 3 } ), "solution( new int[]{ 1, 2, 3 } )" );
		assertEquals( 1, solution( new int[]{ -1, -3 } ), "solution( new int[]{ -1, -3 } )" );
		assertEquals( 1, solution( new int[]{ -1, -3, 3, 4 } ), "solution( new int[]{ -1, -3, 3, 4 } )" );
		assertEquals( 2, solution( new int[]{ -1, -3, 1 } ), "solution( new int[]{ -1, -3, 1 } )" );
		assertEquals( 2, solution( new int[]{ 1 } ), "solution( new int[]{ 1 } )" );
		assertEquals( 1, solution( new int[]{ 2 } ), "solution( new int[]{ 2 } )" );
    }
}

