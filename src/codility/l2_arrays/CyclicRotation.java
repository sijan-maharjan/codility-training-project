package codility.l2_arrays;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * <h1>CyclicRotation</h1>
 * Rotate an array to the right by a given number of steps.<br>
 * 
 * @see <a href="https://app.codility.com/programmers/lessons/2-arrays/cyclic_rotation/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training2YEBKT-GNF/">Codility report</a>
 * @author Sijan
 * @since 2022-05-05
 */
public class CyclicRotation {

    public int[] solution(int[] A, int K) {
        return A.length == 0
                ? A
                : ((K % A.length) == 0)
                    ? A
                    : Stream.of(
                        Arrays.copyOfRange(A, offset(K, A.length), A.length),
                        Arrays.copyOfRange(A, 0, offset(K, A.length))
                ).flatMap(Stream::of).flatMapToInt(IntStream::of).toArray();
    }
    private static int offset(int K, int L){
        return L - (K % L);
    }

    @Test
    public void run( ) {
        /* test-cases */
    }
}

