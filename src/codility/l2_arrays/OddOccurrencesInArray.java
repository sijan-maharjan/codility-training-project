package codility.l2_arrays;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * <h1>OddOccurrencesInArray</h1>
 * Find value that occurs in odd number of elements.<br>
 * Detected time complexity: O(N) or O(N*log(N))
 * @see <a href="https://app.codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training8H2PJK-5Z6/">Codility report</a>
 * @author Sijan
 * @since 2022-05-05
 */
public class OddOccurrencesInArray {

    public int solution(int[] A) {
        Arrays.sort(A);
        for(int i=0; i<A.length; i+=2){
            if(i+1 == A.length) return A[i];
            else if(A[i] - A[i+1] != 0) return A[i];
        }
        throw new RuntimeException("No Odd Occurrence");
    }

    @Test
    public void run( ) {
        /* test-cases */
    }
}

