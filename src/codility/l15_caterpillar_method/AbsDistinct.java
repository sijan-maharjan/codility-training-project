package codility.l15_caterpillar_method;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>AbsDistinct</h1>
 * Compute number of distinct absolute values of sorted array elements.<br>
 * <br>
 * Detected time complexity: O(N) or O(N*log(N))<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/15-caterpillar_method/abs_distinct/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingPH9TS2-Z9C/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-10-04
 */
public class AbsDistinct {

    public int solution(int[] A) {
        return (int) Arrays.stream(A).parallel( ).map(Math::abs).distinct().count();
    }

    @Test
    public void run( ) {
		assertEquals( 5, solution( new int[]{-5,-3,-1,0,3,6} ), "solution( new int[]{-5,-3,-1,0,3,6} )" );
    }
}

