package codility.l7_stacks_and_queues;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>StoneWall</h1>
 * Cover "Manhattan skyline" using the minimum number of rectangles.<br>
 * <br>
 * Detected time complexity: O(N)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/7-stacks_and_queues/stone_wall/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingRUST6K-YEK/#">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-06-05
 */
public class StoneWall {

    public int solution(int[] H) {
        int c = 1;
        Stack<Integer> p = new Stack<>();
        p.push( H[0] );
        for( int i = 1; i< H.length; i++){
            int x = -1;
            if( H[i] < H[i-1] ) {
                while( !p.isEmpty() && p.peek( ) >= H[i]){
                    x = p.pop( );
                }
            }
            if( H[i] != x && H[i] != H[i-1] ) {
                c ++;
            }
            p.push( H[i] );
        }
        return c;
    }

    @Test
    public void run( ) {
		assertEquals( 7, solution( new int[]{ 8, 8, 5, 7, 9, 8, 7, 4, 8 } ), "solution( new int[]{ 8, 8, 5, 7, 9, 8, 7, 4, 8 } )" );
		assertEquals( 8, solution( new int[]{ 8, 8, 5, 7, 9, 8, 9, 7, 4, 8 } ), "solution( new int[]{ 8, 8, 5, 7, 9, 8, 9, 7, 4, 8 } )" );
    }
}

