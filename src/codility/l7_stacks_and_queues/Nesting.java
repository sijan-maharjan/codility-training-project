package codility.l7_stacks_and_queues;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>Nesting</h1>
 * Determine whether a given string of parentheses (single type) is properly nested.<br>
 * <br>
 * Detected time complexity: O(N)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/7-stacks_and_queues/nesting/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingJGPXKM-Q73/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-17
 */
public class Nesting {

    public int solution(String S) {
        Stack<Character> s = new Stack<>();
        for( char c: S.toCharArray() ){
            if (c=='('){
                s.push(')');
            }
            if( c==')' ){
                if( s.isEmpty( ) || s.pop() != c ) return 0;
            }
        }
        return s.isEmpty( )? 1: 0;
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( "(()(())())" ), "solution( \"(()(())())\" )" );
		assertEquals( 0, solution( "())" ), "solution( \"())\" )" );
    }
}

