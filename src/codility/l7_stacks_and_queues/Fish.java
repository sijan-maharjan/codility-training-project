package codility.l7_stacks_and_queues;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>Fish</h1>
 * N voracious fish are moving along a river. Calculate how many fish are alive.<br>
 * <br>
 * <br>
 * <br>
 * Task Score 50%<br>
 * Correctness 50%<br>
 * Performance 50%<br>
 * Total Score 50%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/7-stacks_and_queues/fish/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training9S9DRG-4ZA/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-17
 */
public class Fish {

    public int solution(int[] A, int[] B) {
        int l = A.length;
        Stack<Integer> S = new Stack<>();
        for( int i = 0; i< A.length; i++){
            if( B[i] == 1 ){
                S.push(i);
            } else if( B[i] == 0 && !S.isEmpty() ){
                int x = S.pop();
                if( A[x] > A[i] ) {
                    S.push( x );
                    l--;
                } else {
                    int k = x;
                    boolean a = false;
                    while( x >= 0 ) {
                        if( !a ) k = A[x] > A[i] ? x: i;
                        if( !a && A[x] != A[i] ) l--;
                        else if( !a ) a= true;
                        x = !S.isEmpty() ? S.pop() : -1;
                    }
                    if( B[k] == 1 ){
                        S.push(k);
                    }
                }
            }
        }
        return l;
    }

    @Test
    public void run( ) {
		assertEquals( 2, solution( new int[]{4,3,2,1,5}, new int[]{0,1,0,0,0} ), "solution( new int[]{4,3,2,1,5}, new int[]{0,1,0,0,0} )" );
		assertEquals( 5, solution( new int[]{6,3,2,3,1,5}, new int[]{1,1,0,0,0,0} ), "solution( new int[]{6,3,2,3,1,5}, new int[]{1,1,0,0,0,0} )" );
    }
}

