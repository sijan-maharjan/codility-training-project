package codility.l10_prime_and_composite_numbers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>Flags</h1>
 * Find the maximum number of flags that can be set on mountain peaks.<br>
 * <br>
 * Detected time complexity: O(N) or O(N * sqrt(N))<br>
 * <br>
 * Task Score 80%<br>
 * Correctness 100%<br>
 * Performance 57%<br>
 * Total Score 80%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/10-prime_and_composite_numbers/flags/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingMJTEFS-ZKM/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-24
 */
public class Flags {

    public int solution(int[] A) {
        boolean[] peaks = new boolean[A.length];
        int peakCount = 0;
        for( int i =0; i< A.length; i++ ) {
            peaks[i] = ( i!=0 && i!=A.length-1 && ( A[i+1] - A[i] ) < 0  && ( A[i] - A[i-1] ) > 0 );
            if( peaks[i] ) peakCount ++;
        }
        int flags = peakCount;
        int[] u = new int[]{0, 0};
        int[] max = new int[]{0, 0};
        for( int k = flags; k>=1; k-- ) {
            u = new int[]{0, 0};
            if( max[0] == flags || k < max[0] ) break;
            for( int j=0; j<A.length && u[0] < k; j++ ) {
                if( peaks[j] ){
                    u = new int[]{ u[0]+1, k };
                    j += k-1;
                }
                if( u[0] >= max[0] ) max = u;
            }
        }
        return max[1];
    }

    @Test
    public void run( ) {
		assertEquals( 3, solution( new int[]{ 1,5,3,4,3,4,1,2,3,4,6,2 } ), "solution( new int[]{ 1,5,3,4,3,4,1,2,3,4,6,2 } )" );
		assertEquals( 0, solution( new int[]{ 9 } ), "solution( new int[]{ 9 } )" );
		assertEquals( 0, solution( new int[]{ 9, 10, 20 } ), "solution( new int[]{ 9, 10, 20 } )" );
		assertEquals( 1, solution( new int[]{ 9, 10 ,9 } ), "solution( new int[]{ 9, 10 ,9 } )" );
    }
}

