package codility.l10_prime_and_composite_numbers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>Peaks</h1>
 * Divide an array into the maximum number of same-sized blocks, each of which should contain an index P such that A[P - 1] < A[P] > A[P + 1].<br>
 * <br>
 * Detected time complexity: O(N * log(log(N)))<br>
 * <br>
 * Task Score 81%<br>
 * Correctness 100%<br>
 * Performance 60%<br>
 * Total Score 81%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/10-prime_and_composite_numbers/peaks/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingTFGC2N-FB6/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-24
 */
public class Peaks {

    public int solution(int[] A) {
        List<Integer> peakIndexes = new ArrayList<>( );
        int peakCount = 0;
        for( int i =0; i< A.length; i++ ) {
            boolean peak = ( i!=0 && i!=A.length-1 && ( A[i+1] - A[i] ) < 0  && ( A[i] - A[i-1] ) > 0 );
            if( peak ){
                peakCount ++;
                peakIndexes.add(i);
            }
        }
        LOOP: for( int i = peakCount, j=0; i>1; i--, j=0 ) {
            if( A.length%i != 0 ) continue ;
            float d = ((float) A.length)/((float) i);
            int ds = (int) d;
            if( d!=((float) ds) ) continue;
            for( int n = 0; n<peakCount; n++ ){
                j += ds;
                int k = peakIndexes.get(n);
                if( k >= j ) continue LOOP;
                else while( n+1<peakCount && peakIndexes.get(n+1) < j ) n++;
            }
            return i;
        }
        return peakCount == 0? 0: 1;
    }

    @Test
    public void run( ) {
		assertEquals( 3, solution( new int[]{ 1,2,3,4,3,4,1,2,3,4,6,2 } ), "solution( new int[]{ 1,2,3,4,3,4,1,2,3,4,6,2 } )" );
		assertEquals( 3, solution( new int[]{ 0, 1, 0, 0, 1, 0, 0, 1, 0 } ), "solution( new int[]{ 0, 1, 0, 0, 1, 0, 0, 1, 0 } )" );
		assertEquals( 1, solution( new int[]{ 0, 1, 0, 0, 1, 0, 1 } ), "solution( new int[]{ 0, 1, 0, 0, 1, 0, 1 } )" );
		assertEquals( 0, solution( new int[]{ 9 } ), "solution( new int[]{ 9 } )" );
    }
}

