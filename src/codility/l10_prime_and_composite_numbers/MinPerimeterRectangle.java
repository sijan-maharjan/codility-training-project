package codility.l10_prime_and_composite_numbers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>MinPerimeterRectangle</h1>
 * Find the minimal perimeter of any rectangle whose area equals N.<br>
 * <br>
 * Detected time complexity: O(sqrt(N))<br>
 * <br>
 * Task Score 80%<br>
 * Correctness 100%<br>
 * Performance 60%<br>
 * Total Score 80%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/10-prime_and_composite_numbers/min_perimeter_rectangle/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingMZJEF4-R3P/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-18
 */
public class MinPerimeterRectangle {

    public int solution(int N) {
        int min = Integer.MAX_VALUE;
        Set<Integer> c = new HashSet<>();
        for( int A=1; A<= N; A++ ) {
            if( N % A == 0 ) {
                int B = N/A;
                if( c.contains( B ) ) break;
                int P = 2 * ( A + B );
                if ( P < min ) {
                    min = P;
                }
                c.add( A );
            }
        }
        return min;
    }

    @Test
    public void run( ) {
		assertEquals( 22, solution( 30 ), "solution( 30 )" );
    }
}

