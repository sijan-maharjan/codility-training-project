package codility.l10_prime_and_composite_numbers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>CountFactors</h1>
 * Count factors of given number n.<br>
 * <br>
 * Detected time complexity: O(sqrt(N)) or O(N)<br>
 * <br>
 * Task Score 85%<br>
 * Correctness 100%<br>
 * Performance 66%<br>
 * Total Score 85%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/10-prime_and_composite_numbers/count_factors/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingJ3GQ28-BXV/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-09-18
 */
public class CountFactors {

    public int solution(int N) {
        int count = 0;
        Set<Integer> c = new HashSet<>();
        for( int A=1; A<= N; A++ ) {
            if( N % A == 0 ) {
                int B = N/A;
                if( c.contains( B ) ) break;
                count += 2;
                if( A==B ) count--;
                c.add( A );
            }
        }
        return count;
    }

    @Test
    public void run( ) {
		assertEquals( 8, solution( 24 ), "solution( 24 )" );
		assertEquals( 5, solution( 16 ), "solution( 16 )" );
    }
}

