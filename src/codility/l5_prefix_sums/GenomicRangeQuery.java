package codility.l5_prefix_sums;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>GenomicRangeQuery</h1>
 * Find the minimal nucleotide from a range of sequence DNA.<br>
 * <br>
 * Detected time complexity: O(N + M)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/5-prefix_sums/genomic_range_query/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingM32FC3-JDQ/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-08
 */
public class GenomicRangeQuery {

    public int[] solution(String S, int[] P, int[] Q) {
        int [] answers = new int[P.length];
        int stringLength = S.length();
        int [][] occurrences = new int [stringLength][4];
        int[][] added = new int [stringLength][4];
        for(int i=0; i<occurrences.length; i++) {
            char c = S.charAt(i);
            if(c == 'A')      occurrences[i][0] = 1;
            else if(c == 'C') occurrences[i][1] = 1;
            else if(c == 'G') occurrences[i][2] = 1;
            else if(c == 'T') occurrences[i][3] = 1;
            int j = i - 1;
            if(i>0) {
                added[i][0] = added[j][0] + occurrences[i][0];
                added[i][1] = added[j][1] + occurrences[i][1];
                added[i][2] = added[j][2] + occurrences[i][2];
                added[i][3] = added[j][3] + occurrences[i][3];
            } else {
                added[i][0] = occurrences[i][0];
                added[i][1] = occurrences[i][1];
                added[i][2] = occurrences[i][2];
                added[i][3] = occurrences[i][3];
            }
        }
        for(int i=0; i<P.length; i++) {
            int index1 = P[i];
            int index2 = Q[i];
            int lowerIndexCount = 0;
            for (int j = 0; j < 4; j++) {
                lowerIndexCount = added[index1][j];
                if ( (index1 == index2 && occurrences[index2][j] > 0) || (index1+1 == index2 && added[index2][j] == lowerIndexCount && (occurrences[index1][j] > 0 || occurrences[index2][j] > 0)) || added[index2][j] - lowerIndexCount > 0 || ( added[index2][j] - lowerIndexCount == 0 && occurrences[index1][j] > 0) ) {
                    answers[i] = j + 1;
                    break;
                }
            }
        }
        return answers;
    }

    @Test
    public void run( ) {
		assertArrayEquals( new int[]{2,4,1}, solution( "CAGCCTA", new int[]{ 2,5,0 }, new int[]{4,5,6} ), "solution( \"CAGCCTA\", new int[]{ 2,5,0 }, new int[]{4,5,6} )" );
        assertArrayEquals( new int[]{1,1,2}, solution( "AC", new int[]{ 0,0,1 }, new int[]{0,1,1} ), "solution( \"AC\", new int[]{ 0,0,1 }, new int[]{0,1,1} )" );
        assertArrayEquals( new int[]{3,3,4}, solution( "GT", new int[]{ 0, 0, 1 }, new int[]{0,1,1} ), "solution( \"GT\", new int[]{ 0, 0, 1 }, new int[]{0,1,1} )" );
    }
}

