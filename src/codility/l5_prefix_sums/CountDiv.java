package codility.l5_prefix_sums;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>CountDiv</h1>
 * Compute number of integers divisible by k in range [a..b].<br>
 * Detected time complexity: O(1)
 * @see <a href="https://app.codility.com/programmers/lessons/5-prefix_sums/count_div/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingX46K2S-26X/">Codility report</a>
 * @author Sijan
 * @since 2022-05-05
 */
public class CountDiv {

    public int solution(int A, int B, int K) {
        return (B/K) - ((A==0)?-1: ((A-1)/K));
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( 0, 1, 11 ), "solution( 0, 1, 11 )" );
    }
}

