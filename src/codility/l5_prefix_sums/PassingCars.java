package codility.l5_prefix_sums;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>PassingCars</h1>
 * Count the number of passing cars on the road.<br>
 * <br>
 * Detected time complexity: O(N)<br>
 * <br>
 * Task Score 100%<br>
 * Correctness 100%<br>
 * Performance 100%<br>
 * Total Score 100%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/5-prefix_sums/passing_cars/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingS9TYFK-4E7/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-07
 */
public class PassingCars {

    public int solution(int[] A) {
        int N = A.length;
        long pair = 0, p_count = 0;
        for( int x = 0; x < N ; x++ ){
            if( A[x] == 0 ){
                p_count++;
            } else {
                pair += p_count;
            }
        }
        return pair > 1000000000 ? -1 : ( int ) pair;
    }

    @Test
    public void run( ) {
		assertEquals( 5, solution( new int[]{ 0,1,0,1,1 } ), "solution( new int[]{ 0,1,0,1,1 } )" );
    }
}

