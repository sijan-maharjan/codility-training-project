package codility.l5_prefix_sums;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



/**
 * <h1>MinAvgTwoSlice</h1>
 * Find the minimal average of any slice containing at least two elements.<br>
 * <br>
 * Detected time complexity: O(N)<br>
 * <br>
 * Task Score 80%<br>
 * Correctness 80%<br>
 * Performance 80%<br>
 * Total Score 80%<br>
 * <br> [modified - same result]
 * @see <a href="https://app.codility.com/programmers/lessons/5-prefix_sums/min_avg_two_slice/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/trainingKSQN6Y-ECV/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-05-10
 */
public class MinAvgTwoSlice {

    public int solution(int[] A) {
        int s = A.length;
        double minAv = Double.MAX_VALUE; double minSt = s;
        int [] sums = new int[s>3?(s/2)-1:0];
        double X = A[0];
        double Y = A[s-1];
        int sum = A[0];
        for( int i=1; i<s; i++ ){
            if( s> 2) {
                double av1 = ((double) A[i]+A[i-1])/(double) 2;
                if( av1 < minAv ){
                    minAv = av1;
                    minSt = i-1;
                } else if( av1 == minAv && i-1 < minSt ){
                    minSt = i-1;
                }
                if(i<s-1) {
                    X = ((X * i) + A[i]) / ((double) i + 1);
                    if (X <= minAv) {
                        minAv = X;
                        minSt = 0;
                    }
                    Y = ((Y * i) + A[s - i - 1]) / ((double) i + 1);
                    if (Y < minAv) {
                        minAv = Y;
                        minSt = s - i - 1;
                    } else if (Y == minAv && s - i - 1 < minSt) {
                        minSt = s - i - 1;
                    }
                }
            }
            if(s>3) {
                if (i <= (s / 2) - 1) {
                    if (i - 1 > 0) sums[i - 1] = sums[i - 2];
                    sums[i - 1] -= (A[i - 1] + A[s - i]);
                }
                sum += A[i];
            }
        }
        double avg = ((double) sum)/(double) s;
        if( avg <= minAv ){
            minAv = avg;
            minSt = 0;
        }
        for( int i = 0; s>3 && i<sums.length;i++ ){
            double av = ((double) sum+sums[i])/((double) s-((i+1)*2));
            if( av < minAv ){
                minAv = av;
                minSt = i+1;
            } else if( av == minAv && i+1 < minSt ){
                minSt = i+1;
            }
        }
        return ( int ) minSt;
    }

    @Test
    public void run( ) {
		assertEquals( 1, solution( new int[]{4,2,2,5,1,5,8} ), "solution( new int[]{4,2,2,5,1,5,8} )" );
		assertEquals( 0, solution( new int [] { 10000, -10000 } ), "solution( new int[]{4,2,2,5,1,5,8} )" );
		assertEquals( 2, solution( new int [] { 50000 , 20000, -10000, 10000, 10000, -10000, 20000, 50000 } ), "solution( new int[]{4,2,2,5,1,5,8} )" );
    }
}

