package codility.l13_fibonacci_numbers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * <h1>FibFrog</h1>
 * Count the minimum number of jumps required for a frog to get to the other side of a river.<br>
 * <br>
 * <br>
 * <br>
 * Task Score 41%<br>
 * Correctness 83%<br>
 * Performance 0%<br>
 * Total Score 41%<br>
 * <br>
 * @see <a href="https://app.codility.com/programmers/lessons/13-fibonacci_numbers/fib_frog/">Problem Detail</a>
 * @see <a href="https://app.codility.com/demo/results/training84PNC8-RTJ/">Codility report</a>
 * @version 2.0
 * @author Sijan
 * @since 2022-10-01
 */
public class FibFrog {

    public int solution( int [] A ) {
        return solution( A, 0 );
    }
    public int solution( int [] A, int k ) {
        int N = A.length;
        int c = 1;
        Stack<Integer> path = new Stack<>();
        int j = 1;
        for( int i = k; i<=N; i++, j++ ){
            if( ( i==N || A[i] == 1 ) && isFibonacci( j ) ) {
                path.add( i );
            }
        }
        int l = -1;
        while (!path.isEmpty()) {
            int m = path.pop();
            if (m == N) {
                l=c;
                break;
            }
            l = solution(A, m+1);
            if( l!= -1) {
                c += l;
                break;
            }
        }
        return l==-1?-1:c;
    }
    private boolean isPerfectSquare( int x )
    {
        int s = ( int ) Math.sqrt( x );
        return ( s*s == x );
    }
    private boolean isFibonacci( int n )
    {
        // n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
        // is a perfect square
        return isPerfectSquare( 5*n*n + 4 ) ||
                isPerfectSquare( 5*n*n - 4 );
    }

    @Test
    public void run( ) {
		assertEquals( 3, solution( new int[]{0,0,0,1,1,0,1,0,0,0,0} ), "solution( new int[]{0,0,0,1,1,0,1,0,0,0,0} )" );
		assertEquals( 2, solution( new int[]{1,1,1} ), "solution( new int[]{1,1,1} )" );
		assertEquals( -1, solution( new int[]{0,0,0} ), "solution( new int[]{0,0,0} )" );
    }
}

